package com.cj.sale.util;

/**统一返回结果类*/
public class Result<T> {
    private boolean success;
    private int code;

    private String msg;

    private T data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Result() {

    }
    public Result(boolean success, String msg, T data) {
		super();
        if (success){
            this.code = 200;
        }else {
            this.code = 999;
        }
		this.msg = msg;
		this.data = data;
	}

    public Result(boolean success, String msg) {
        super();
        if (success){
            this.code = 200;
        }else {
            this.code = 999;
        }
        this.msg = msg;
    }
}

package com.cj.sale.cache;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;

@Configuration
public class RedisConfig extends RedisStandaloneConfiguration {

    @Value("${spring.cache.redis.host}")
    private String redisHost;

    @Value("${spring.cache.redis.port}")
    private int redisPort;

    @Value("${spring.cache.redis.database}")
    private int database;

    @Override
    public void setDatabase(int dbIndex) {
        this.setDatabase(this.database);
    }

    @Override
    public int getDatabase() {
        return this.database;
    }

    @Override
    public void setHostName(String hostName) {
        this.setHostName(this.redisHost);
    }

    @Override
    public String getHostName() {
        return this.redisHost;
    }

    @Override
    public void setPort(int port) {
        this.setPort(this.redisPort);
    }

    @Override
    public int getPort() {
        return this.redisPort;
    }
}

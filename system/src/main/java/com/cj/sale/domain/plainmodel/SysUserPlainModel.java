package com.cj.sale.domain.plainmodel;

import com.cj.sale.entity.BaseEntity;

/**用户实体类*/
public class SysUserPlainModel extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String account;
    private String realName;
    private String email;
    private String phone;

    @Override
    public String toString() {
        return "SysUserPlainModel{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}

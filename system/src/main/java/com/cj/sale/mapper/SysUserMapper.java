package com.cj.sale.mapper;

import com.cj.sale.domain.model.SysUserModel;
import com.cj.sale.domain.plainmodel.SysUserPlainModel;
import com.cj.sale.util.Result;
import org.springframework.stereotype.Component;

public interface SysUserMapper {

    int createUser(SysUserModel model);
}

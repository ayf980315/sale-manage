package com.cj.sale.service;

import com.cj.sale.domain.model.SysUserModel;
import com.cj.sale.domain.plainmodel.SysUserPlainModel;
import com.cj.sale.util.Result;

import java.text.ParseException;

public interface SysUserService {
    Result<SysUserPlainModel> getUserById(Long id);

    Result<String> createUser(SysUserModel model);
}

package com.cj.sale.service.impl;

import com.cj.sale.domain.model.SysUserModel;
import com.cj.sale.domain.plainmodel.SysUserPlainModel;
import com.cj.sale.mapper.SysUserMapper;
import com.cj.sale.service.SysUserService;
import com.cj.sale.util.Result;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;
    /**
     * 根据用户ID获取用户信息的简单模型。
     * @param id 用户的唯一标识ID。
     */
    @Override
    public Result<SysUserPlainModel> getUserById(Long id) {
        return null;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result<String> createUser(SysUserModel model) {
        model.setSalt("Aa123456");
        model.setCreateBy("admin");
        if (Objects.isNull(model.getValid())){
            model.setValid(1);
        }
        model.setCreateTime(new Date());
        boolean created = sysUserMapper.createUser(model)>0;
        if (created){
            return new Result<>(true,"创建成功");
        }
        return new Result<>(false,"创建失败");
    }
}

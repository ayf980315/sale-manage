package com.cj.sale.controller;

import com.cj.sale.domain.model.SysUserModel;
import com.cj.sale.domain.plainmodel.SysUserPlainModel;
import com.cj.sale.service.SysUserService;
import com.cj.sale.util.Result;
import org.springframework.web.bind.annotation.*;
import jakarta.annotation.Resource;

@RestController
@RequestMapping("/user")
public class SysUserController {

    @Resource
    private SysUserService sysUserService;


    @GetMapping("/get/by/id")
    public Result<SysUserPlainModel> getUserById(Long id){
        return sysUserService.getUserById(id);
    }

    @PostMapping("/create")
    public Result<String> createUser(@RequestBody SysUserModel model){
        return sysUserService.createUser(model);
    }
}

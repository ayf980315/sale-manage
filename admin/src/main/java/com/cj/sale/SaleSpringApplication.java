package com.cj.sale;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.cj.sale"})
@MapperScan("com.cj.sale.mapper")
public class SaleSpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(SaleSpringApplication.class, args);
        System.out.println("~~~~~~~~~~~启动成功~~~~~~~~~~~~");
    }
}
